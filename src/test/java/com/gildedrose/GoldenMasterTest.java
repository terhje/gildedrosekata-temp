package com.gildedrose;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.approvaltests.Approvals;
import org.junit.jupiter.api.Test;

public class GoldenMasterTest {

  public static final int FIXED_SEED = 100;
  public static final int UPPER_BOUND = 101;
  public static final int LOWER_BOUND = -50;
  public static final int NUMBER_OF_ITEMS = 2000;

  private final Random random = new Random(FIXED_SEED);

  private final List<String> itemNames =
      List.of(
          "+5 Dexterity Vest",
          "Aged Brie",
          "Elixir of the Mongoose",
          "Moongrass",
          "Sulfuras, Hand of Ragnaros",
          "Backstage passes to a TAFKAL80ETC concert");

  @Test
  void goldenMaster() {
    Item[] items = generateRandomItems();
    GildedRose gildedRose = new GildedRose(items);
    gildedRose.updateQuality();

    Approvals.verify(generateStringRepresentation(items));
  }

  private String generateStringRepresentation(Item[] items) {
    return Arrays.stream(items).map(item -> item + "\n").collect(Collectors.joining());
  }

  private Item[] generateRandomItems() {
    return IntStream.range(0, NUMBER_OF_ITEMS)
        .mapToObj(i -> new Item(generateItemName(), generateSellin(), generateQuality()))
        .toArray(Item[]::new);
  }

  private String generateItemName() {
    return itemNames.get(random.nextInt(itemNames.size()));
  }

  private int generateQuality() {
    return randomNumber();
  }

  private int randomNumber() {
    return random.nextInt(LOWER_BOUND, UPPER_BOUND);
  }

  private int generateSellin() {
    return randomNumber();
  }
}
